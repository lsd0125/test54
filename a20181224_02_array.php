<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>123</title>
</head>
<body>
<pre>
<?php

$ar = array( 2, 3, 4, 5, 6);

// 關聯式陣列
$ar2 = [
    'name' => 'shinder',
    'age' => 30,
    'gender' => 'male',
    100, // 避免混用
];

echo $ar2['name']. "\n";

//$ar3 = $ar2; // 設定值 (考備 值)
$ar3 = &$ar2; // 傳遞位址設定

$ar2['name'] = 'bill';


print_r($ar2);
print_r($ar3);
?>
</pre>
</body>
</html>





