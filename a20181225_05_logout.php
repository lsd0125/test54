<?php
    session_start(); // 啟用 session 功能

    unset($_SESSION['user']); // 清除 session 變數

    // 轉向, redirect
    header('Location: a20181225_04_login.php');