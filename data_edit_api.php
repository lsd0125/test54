<?php

require __DIR__. '/__connect_db.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
    'postData' => [],
];


if(isset($_POST['sid']) and isset($_POST['name']) and isset($_POST['email'])){

    $result['postData'] = $_POST;

    // 不是要修改的這一筆, 若找到相同的 email
    $s_sql = "SELECT 1 FROM `address_book` WHERE `email`=? AND `sid`<> ?";
    $s_stmt = $pdo->prepare($s_sql);
    $s_stmt->execute([
        $_POST['email'],
        $_POST['sid'],
    ]);
    if($s_stmt->rowCount()==1){
        $result['code'] = 420;
        $result['info'] = 'Email 重複';
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    // \[value\-\d\]
    $sql = "UPDATE `address_book` SET `name`=?,`email`=?,`mobile`=?,`address`=?,`birthday`=? WHERE `sid`=?";
    
    $stmt = $pdo->prepare($sql);
    
    $stmt->execute([
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['address'],
        $_POST['birthday'],
        $_POST['sid']
    ]);

    // 影響的列數 (筆數)
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 200;
        $result['info'] = '資料修改完成';
    } else {
        $result['code'] = 410;
        $result['info'] = '資料沒有修改';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);









