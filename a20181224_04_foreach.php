<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>123</title>
</head>
<body>
<pre>
<?php
// 關聯式陣列
$ar2 = [
    'name' => 'shinder',
    'age' => 30,
    'gender' => 'male',
    100, // 避免混用
];

foreach($ar2 as $k => $v){
    echo " $k : $v <br>";
}
echo '-----<br>';
foreach($ar2 as $a => $b){
    echo " $a : $b <br>";
}
echo '-----<br>';
foreach($ar2 as $i){
    echo " $i <br>";
}

?>
</pre>
</body>
</html>





