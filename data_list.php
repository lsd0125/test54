<?php
require __DIR__. '/__connect_db.php';
$page_name = 'data_list';

$per_page = 5; // 每頁幾筆

$page = isset($_GET['page']) ? intval($_GET['page']) : 1; // 用戶要看第幾頁

$t_sql = "SELECT COUNT(1) FROM address_book";
// 算總筆數

//$t_stmt = $pdo->query($t_sql);
//$t_row = $t_stmt->fetch(PDO::FETCH_NUM);
//$total = $t_row[0];

$total = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];
$total_pages = ceil($total/$per_page); // 總頁數

$page = $page>$total_pages ? $total_pages : $page;
$page = $page<1 ? 1 : $page;


$sql = sprintf("SELECT 
          `sid`, `name`, `email`, `mobile`, `address`, `birthday`
        FROM address_book
        ORDER BY `sid` DESC
        LIMIT %s, %s", ($page-1)*$per_page, $per_page);

$stmt = $pdo->query($sql);

//$row = $stmt->fetch(PDO::FETCH_ASSOC);
// $ar = $stmt->fetchAll(PDO::FETCH_ASSOC); // 一次讀取全部

?>
<?php include __DIR__. '/__html_head.php' ?>

<?php include __DIR__. '/__navbar.php' ?>

<div class="container">

    <div>
        <?= " $page / $total_pages" ?>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <nav >
                <ul class="pagination">
                    <?php if($page==1): ?>
                        <li class="page-item">
                            <a class="page-link" href="javascript:">
                                <i class="fas fa-arrow-alt-circle-left"></i>
                            </a>
                        </li>
                    <?php else: ?>
                        <li class="page-item">
                            <a class="page-link" href="?page=<?= $page-1 ?>">
                                <i class="fas fa-arrow-alt-circle-left"></i>
                            </a>
                        </li>
                    <?php endif ?>

                    <?php for($i=1; $i<=$total_pages; $i++): ?>
                    <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                        <a class="page-link" href="?page=<?=$i?>"><?= $i ?></a>
                    </li>
                    <?php endfor ?>
                    <li class="page-item">
                        <a class="page-link" <?=
                        $page==$total_pages ? '' : sprintf('href="?page=%s"', $page+1)
                        ?> >
                            <i class="fas fa-arrow-alt-circle-right"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr class="table-success">
            <th scope="col"><i class="fas fa-trash-alt"></i></th>
            <th scope="col">#</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
            <th scope="col">mobile</th>
            <th scope="col">address</th>
            <th scope="col">birthday</th>
            <th scope="col"><i class="fas fa-edit"></i></th>
        </tr>
        </thead>
        <tbody>
        <?php while($r=$stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><a href="javascript: delete_it(<?= $r['sid'] ?>)"><i class="fas fa-trash-alt"></i></a>
            </td>
            <td><?= $r['sid'] ?></td>
            <td><?= htmlentities($r['name']) ?></td>
            <td><?= htmlentities($r['email']) ?></td>
            <td><?= htmlentities($r['mobile']) ?></td>
            <td><?= htmlentities($r['address']) ?></td>
            <td><?= $r['birthday'] ?></td>
            <td><a href="data_edit.php?sid=<?= $r['sid'] ?>"><i class="fas fa-edit"></i></a></td>

        </tr>
        <?php endwhile ?>
        </tbody>
    </table>
    <div class="row">
        <div class="col-lg-12">
            <nav >
                <ul class="pagination">
                    <?php for($i=1; $i<=$total_pages; $i++): ?>
                        <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                            <a class="page-link" href="?page=<?=$i?>"><?= $i ?></a>
                        </li>
                    <?php endfor ?>

                </ul>
            </nav>
        </div>
    </div>
</div>

    <script>
        function delete_it(sid){
            if(confirm('刪除編號為 ' + sid + ' 的資料?')){
                location.href = 'data_delete.php?sid=' + sid;
            }
        }
    </script>
<?php include __DIR__. '/__html_foot.php' ?>