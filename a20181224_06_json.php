<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>123</title>
</head>
<body>
<pre>
<?php

$str = '{"name":"史瑞克","age":30,"gender":"male"}';

$a = json_decode($str);
var_dump($a);
echo $a->name. '<br>';

$b = json_decode($str, true);
var_dump($b);
echo $b['name']. '<br>';

?>
</pre>


</body>
</html>





