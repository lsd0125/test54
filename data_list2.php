<?php
require __DIR__. '/__connect_db.php';
$page_name = 'data_list';

$sql = "SELECT `sid`, `name`, `email`, `mobile`, `address`, `birthday` FROM address_book";

$stmt = $pdo->query($sql);

//$row = $stmt->fetch(PDO::FETCH_ASSOC);
// $ar = $stmt->fetchAll(PDO::FETCH_ASSOC); // 一次讀取全部

?>
<?php include __DIR__. '/__html_head.php' ?>

<?php include __DIR__. '/__navbar.php' ?>

<div class="container">

    <table class="table table-bordered table-striped">
        <thead>
        <tr class="table-success">
            <th scope="col">#</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
<!--            <th scope="col">mobile</th>-->
            <th scope="col">address</th>
            <th scope="col">birthday</th>
        </tr>
        </thead>
        <tbody>
        <?php while($r=$stmt->fetch(PDO::FETCH_NUM)): ?>
        <tr>
            <td><?= $r[0] ?></td>
            <td><?= $r[1] ?></td>
            <td><?= $r[2] ?></td>
<!--            <td>--><?//= $r[3] ?><!--</td>-->
            <td><?= $r[4] ?></td>
            <td><?= $r[5] ?></td>

        </tr>
        <?php endwhile ?>
        </tbody>
    </table>

</div>


<?php include __DIR__. '/__html_foot.php' ?>