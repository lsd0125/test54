<?php
require __DIR__. '/__connect_db.php';

$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if(empty($sid)){
    header('Location: ./data_list.php');
    exit;
}

$sql = "DELETE FROM `address_book` WHERE `sid`=$sid";

$pdo->query($sql);

if(isset($_SERVER['HTTP_REFERER'])){
    // 從哪裡來回哪裡去
    header('Location: '. $_SERVER['HTTP_REFERER']);
} else {
    header('Location: ./data_list.php');
}


