<?php
//require __DIR__. '/__connect_db.php';
$page_name = 'data_add';

?>
<?php include __DIR__. '/__html_head.php' ?>

<?php include __DIR__. '/__navbar.php' ?>
    <style>
        small {
            color: red;
        }
    </style>
<div class="container">
    <div class="row">
        <div class="col-lg-6">

            <div id="info" class="alert" role="alert" style="display: none">

            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">新增資料</h5>

                    <form name="form1" method="post" onsubmit="return formCheck();">
                        <div class="form-group">
                            <label for="name">姓名</label>
                            <input type="text" class="form-control" id="name" name="name">
                            <small id="nameHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="email">電子郵箱</label>
                            <input type="text" class="form-control" id="email" name="email">
                            <small id="emailHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                            <small id="mobileHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday">
                            <small id="birthdayHelp" class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" name="address" id="address" cols="30" rows="3"></textarea>
                            <small id="addressHelp" class="form-text"></small>
                        </div>

                        <button type="submit" class="btn btn-primary">新增</button>
                    </form>

                </div>
            </div>
        </div>

    </div>

</div>
    <script>
        // ^[A-Za-z]{2}\d{8}$ // 統一發票
        // ^09\d{2}\-?\d{3}\-?\d{3}$ // 手機號碼

        var fields = ['name', 'email', 'mobile', 'address', 'birthday'];
        var i, s;
        var info = $('#info');

        function formCheck(){
            info.hide();
            // 讓每一欄都恢復原來的狀態
            for(s in fields){
                cancelAlert(fields[s]);
            }

            var isPass = true;
            var email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if(document.form1.name.value.length < 2){
                setAlert('name', '請輸入正確的姓名');
                isPass = false;
            }

            if(! email_pattern.test(document.form1.email.value)){
                setAlert('email', '請輸入正確的 email 格式');
                isPass = false;
            }

            if(isPass){

                $.post('data_add_api.php', $(document.form1).serialize(), function(data){
                    var alertType = 'alert-danger';
                    // if(data.info){
                    //     alert( data.info );
                    // }

                    info.removeClass('alert-danger');
                    info.removeClass('alert-success');

                    if(data.success){
                        alertType = 'alert-success';
                    } else {
                        alertType = 'alert-danger';
                    }
                    info.addClass(alertType);
                    if(data.info){
                        info.html( data.info );
                        info.slideDown();
                    }


                }, 'json');

            }

            return false;
        }

        // 設定警示
        function setAlert(fieldName, msg){
            $('#'+fieldName).css('border', '1px solid red');
            $('#'+fieldName+'Help').text(msg);
        }

        // 取消警示
        function cancelAlert(fieldName){
            $('#'+fieldName).css('border', '1px solid #cccccc');
            $('#'+fieldName+'Help').text('');
        }
    </script>

<?php include __DIR__. '/__html_foot.php' ?>